using System;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace SmsSender
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class Settings : ContentPage
    {
        public Settings()
        {
            InitializeComponent();

            if(Application.Current.Properties.ContainsKey("oib"))
                oibEntry.Text = (string)Application.Current?.Properties["oib"];
            if(Application.Current.Properties.ContainsKey("password"))
                passwordEntry.Text = (string)Application.Current?.Properties["password"];
            if (Application.Current.Properties.ContainsKey("poslovni_prostor"))
                poslovniProstor.Text = (string)Application.Current?.Properties["poslovni_prostor"];
        }

        async void SaveCredentials(object sender, EventArgs e)
        {
            Application.Current.Properties ["oib"] = oibEntry.Text;
            Application.Current.Properties["password"] = passwordEntry.Text;
            Application.Current.Properties["poslovni_prostor"] = poslovniProstor.Text;

            await Navigation.PopAsync();
        }
    }
}