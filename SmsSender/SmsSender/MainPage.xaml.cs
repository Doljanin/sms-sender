﻿using System;
using System.Collections.ObjectModel;
using System.Threading.Tasks;
using Plugin.Messaging;
using SmsSender.Model;
using Xamarin.Essentials;
using Xamarin.Forms;

namespace SmsSender
{
    public partial class MainPage : ContentPage
    {
        private readonly MainPageViewModel _vm = new MainPageViewModel();
        private int _messageIndex = 0;

        public MainPage()
        {
            InitializeComponent();

            BindingContext = _vm;
            CrossMessaging.Current.SmsMessenger.OnSmsDeliveryResult += OnSmsDeliveryResult;
        }

        private async void GetData(object sender, EventArgs eventArgs)
        {
            try
            {
                SendBtn.IsEnabled = false;
                _vm.Messages.Clear();
            
                var api = new ApiClient();
                var messages =  api.GetSms();

                foreach (var message in messages)
                {
                    _vm.Messages.Add(message);
                }

                if (messages.Count == 0)
                {
                    await DisplayAlert("Upozorenje", "Nema novih poruka", "OK");
                }
                else
                {
                    SendBtn.IsEnabled = true;
                }
            }
            catch (Exception e)
            {
                await DisplayAlert("Upozorenje", e.Message, "OK");
            }
        }
        
        private async void OnSmsDeliveryResult(bool isSuccessful)
        {
            if (isSuccessful)
            {
                var api = new ApiClient();
                api.ConfirmSmsSent(_vm.Messages[_messageIndex].UserName);
                
                if (_messageIndex == _vm.Messages.Count - 1)
                {
                    await DisplayAlert("Slanje uspješno", "Poslane su sve poruke", "OK");
                    ResetState();
                }
                else
                {
                    _messageIndex++;
                    SendSms();
                }
            }
            else
            {
                await DisplayAlert("Pogreška", "Došlo je do pogreške prilikom slanja poruke", "OK");
                ResetState();
            }
        }

        private void ResetState()
        {
            _vm.Messages.Clear();
            _messageIndex = 0;
        }

        private async void SendSms()
        {
                try
                {
                    var message = _vm.Messages[_messageIndex];
                    var smsMessenger = CrossMessaging.Current.SmsMessenger;
                    if (smsMessenger.CanSendSmsInBackground)
                        smsMessenger.SendSmsInBackground(message.PhoneNumber, message.MessageText);
                    else
                    {
                        await DisplayAlert("Pogreška", "Aplikacija nema prava za slanje SMS poruka!", "OK");
                        ResetState();
                    }
                }
                catch (Exception e)
                {
                    await DisplayAlert("Pogreška", "Pogreška prilikom slanja poruke!!", "OK");
                    ResetState();
                }
        }

        private async void StartSendingSms(object sender, EventArgs eventArgs)
        {
            var permissionStatus = await CheckAndRequestSendSmsPermissions();
                if (permissionStatus == PermissionStatus.Granted)
                {
                    SendBtn.IsEnabled = false;
                    SendSms();
                }
                else
                {
                    await DisplayAlert("Pogreška", "Aplikacija nema prava za slanje SMS poruka!", "OK");
                    _vm.Messages = new ObservableCollection<SmsDto>();
                }
        }

        public async Task<PermissionStatus> CheckAndRequestSendSmsPermissions()
        {
            var readWritePermission = DependencyService.Get<ISendSmsPermission>();
            var status = await readWritePermission.CheckStatusAsync();

            if (status != PermissionStatus.Granted)
            {
                status = await readWritePermission.RequestAsync();
            }

            return status;
        }
        
        private async void NavigateToSettings(object sender, EventArgs e) {  
            await Navigation.PushAsync(new Settings());  
        }  
    }
}