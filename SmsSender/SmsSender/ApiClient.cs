using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Newtonsoft.Json;
using Newtonsoft.Json.Serialization;
using SmsSender.Model;
using SmsSender.ProduxLic;
using Xamarin.Forms;

namespace SmsSender
{
    public class ApiClient
    {
        public List<SmsDto> GetSms()
        {
            try
            {
                var oib = Application.Current.Properties["oib"] as string;
                var pass = Application.Current.Properties["password"] as string;
                var pp = Application.Current.Properties["poslovni_prostor"] as string ?? "";
                
                var client = new ProduxLicSoapClient(ProduxLicSoapClient.EndpointConfiguration.ProduxLicSoap);
                var smsForSending = client.Produx_SMS_GET_UNSENDED_SMS_V2(oib, pass, pp);

                if (string.IsNullOrEmpty(smsForSending.errorDesc))
                {
                    var smsList = JsonConvert.DeserializeObject<List<SmsDto>>(smsForSending.JsonData);
                    return smsList;
                }
                else
                {
                    throw new Exception(smsForSending.errorDesc);
                }
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
                throw;
            }
        }

        public void ConfirmSmsSent(string smsId)
        {
            try
            {
                var oib = Application.Current.Properties["oib"] as string;
                var pass = Application.Current.Properties["password"] as string;
                
                var client = new ProduxLicSoapClient(ProduxLicSoapClient.EndpointConfiguration.ProduxLicSoap);
                client.Produx_SMS_SEND_CONFIRMATION(oib, pass, smsId);

            }
            catch (Exception e)
            {
                Console.WriteLine(e);
                throw;
            }
        }
    }
}