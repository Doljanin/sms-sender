using System.Threading.Tasks;
using Xamarin.Essentials;

namespace SmsSender
{
    public interface ISendSmsPermission
    {
        Task<PermissionStatus> CheckStatusAsync();
        Task<PermissionStatus> RequestAsync();
    }
}