using System.Collections.ObjectModel;
using System.Linq;
namespace SmsSender.Model
{
    public class MainPageViewModel
    {
        public ObservableCollection<SmsDto> Messages { get; set; } = new ObservableCollection<SmsDto>();
        
        public bool IsSendEnabled => Messages.Any();
    }
}