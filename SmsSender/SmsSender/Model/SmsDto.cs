using Newtonsoft.Json;

namespace SmsSender.Model
{
    public class SmsDto
    {
        
        [JsonProperty("SMS_ID_U")]
        public string UserName { get; set; }

        [JsonProperty("SMS_NUMBER")]
        public string PhoneNumber { get; set; }

        [JsonProperty("SMS_TEXT")]
        public string MessageText { get; set; }

        [JsonProperty("SMS_SUBJECT")]
        public string SmsSubject { get; set; }
    }
}