using System.Collections.Generic;
using Xamarin.Essentials;

namespace SmsSender.Droid
{
    public class SendSmsPermission : Permissions.BasePlatformPermission, ISendSmsPermission
    {
        public override (string androidPermission, bool isRuntime)[] RequiredPermissions => new List<(string androidPermission, bool isRuntime)>
        {
            (Android.Manifest.Permission.SendSms, true),
            (Android.Manifest.Permission.ReadPhoneState, true)
        }.ToArray();
    }
}